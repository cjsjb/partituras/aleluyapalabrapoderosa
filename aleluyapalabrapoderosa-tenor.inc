\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key c \major

		R1*2  |
		e 2 c  |
		g 2 e  |
%% 5
		R1*2  |
		e 2 c  |
		g 2 e  |
		r4 a a a  |
%% 10
		g 4 ( f ) e f  |
		g 1  |
		R1  |
		\times 2/3 { e 4 e 8 } \times 2/3 { e 4 e 8 } \times 2/3 { e 4 e 8 } \times 2/3 { e 4 e 8 }  |
		\times 2/3 { e 4 e 8 } \times 2/3 { e 4 e 8 } \times 2/3 { e 4 e 8 } \times 2/3 { e 4 e 8 }  |
%% 15
		\times 2/3 { f 4 f 8 } \times 2/3 { f 4 f 8 } \times 2/3 { f 4 f 8 } \times 2/3 { f 4 f 8 }  |
		g 4 f e d  |
		\times 2/3 { c' 4 c' 8 } \times 2/3 { c' 4 c' 8 } \times 2/3 { c' 4 c' 8 } \times 2/3 { c' 4 c' 8 }  |
		\times 2/3 { b 4 b 8 } \times 2/3 { b 4 b 8 } \times 2/3 { b 4 b 8 } \times 2/3 { b 4 b 8 }  |
		\times 2/3 { a 4 a 8 } \times 2/3 { a 4 a 8 } \times 2/3 { a 4 a 8 } \times 2/3 { a 4 a 8 }  |
%% 20
		b 4 b b b  |
		c' 2 g  |
		b 2 g  |
		R1*2  |
%% 25
		e 2 c  |
		g 2 e  |
		r4 a a a  |
		g 4 ( f ) e f  |
		g 1  |
%% 30
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		¡A -- le -- lu -- ya!
		¡A -- le -- lu -- ya, a -- le -- lu -- ya __ al Se -- ñor!

		Tu pa -- la -- "bra es" po -- de -- ro -- sa,
		tu pa -- la -- bra es e -- ter -- na,
		tu pa -- la -- "bra es" u -- "na es" -- pa -- da de dos fi -- los.

		Tu pa -- la -- "bra es" po -- de -- ro -- sa,
		tu pa -- la -- bra es e -- ter -- na,
		tu pa -- la -- "bra es" u -- "na es" -- pa -- da de dos fi -- los.

		¡A -- le -- lu -- ya!
		¡A -- le -- lu -- ya, a -- le -- lu -- ya __ al Se -- ñor!
	}

>>
