\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key c \major

		R1*2  |
		e' 2 c'  |
		g' 2 e'  |
%% 5
		r4 f' e' d'  |
		e' 4 ( d' ) c' d'  |
		e' 1  |
		R1  |
		r4 f' e' d'  |
%% 10
		e' 4 ( d' ) c' d'  |
		c' 1  |
		R1  |
		\times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 }  |
		\times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 }  |
%% 15
		\times 2/3 { f' 4 f' 8 } \times 2/3 { f' 4 f' 8 } \times 2/3 { f' 4 f' 8 } \times 2/3 { f' 4 f' 8 }  |
		g' 4 f' e' d'  |
		\times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 }  |
		\times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 } \times 2/3 { e' 4 e' 8 }  |
		\times 2/3 { f' 4 f' 8 } \times 2/3 { f' 4 f' 8 } \times 2/3 { f' 4 f' 8 } \times 2/3 { f' 4 f' 8 }  |
%% 20
		g' 4 f' e' d'  |
		e' 2 c'  |
		g' 2 e'  |
		r4 f' e' d'  |
		e' 4 ( d' ) c' d'  |
%% 25
		e' 1  |
		R1  |
		r4 f' e' d'  |
		e' 4 ( d' ) c' d'  |
		c' 1  |
%% 30
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		¡A -- le -- lu -- ya, a -- le -- lu -- ya __ al Se -- ñor!
		¡A -- le -- lu -- ya __ al Se -- ñor!

		Tu pa -- la -- "bra es" po -- de -- ro -- sa,
		tu pa -- la -- bra es e -- ter -- na,
		tu pa -- la -- "bra es" u -- "na es" -- pa -- da de dos fi -- los.

		Tu pa -- la -- "bra es" po -- de -- ro -- sa,
		tu pa -- la -- bra es e -- ter -- na,
		tu pa -- la -- "bra es" u -- "na es" -- pa -- da de dos fi -- los.

		¡A -- le -- lu -- ya, a -- le -- lu -- ya __ al Se -- ñor!
		¡A -- le -- lu -- ya __ al Se -- ñor!
	}

>>
