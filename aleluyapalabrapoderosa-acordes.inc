	\context ChordNames
		\chords {
		\set chordChanges = ##t

		% intro
		c1 g1

		% aleluya...
		c1 e1:m f1 g1
		c1 e1:m f1 g1

		% separacion
		\bar "||"
		c1 g1

		% tu palabra es poderosa...
		c1 e1:m f1 g1
		c1 e1:m f1 g1

		% aleluya...
		c1 e1:m f1 g1
		c1 e1:m f1 g1
		c1 c1
		}
